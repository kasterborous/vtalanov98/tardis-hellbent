local PART={}
PART.ID = "hellbenthandle4"
PART.Name = "Hell Bent TARDIS Handle 4"
PART.Model = "models/doctorwho1200/hellbent/handle4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.SoundOn = "doctorwho1200/hellbent/physlock_on.wav"
PART.SoundOff = "doctorwho1200/hellbent/physlock_off.wav"

TARDIS:AddPart(PART)