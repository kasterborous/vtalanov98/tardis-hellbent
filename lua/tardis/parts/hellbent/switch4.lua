local PART={}
PART.ID = "hellbentswitch4"
PART.Name = "Hell Bent TARDIS Switch 4"
PART.Model = "models/doctorwho1200/hellbent/switch4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/switch.wav"

TARDIS:AddPart(PART)