local PART={}
PART.ID = "hellbentstasisswitch"
PART.Name = "Hell Bent TARDIS Stasis Switch"
PART.Model = "models/doctorwho1200/hellbent/stasisswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/hellbent/switch.wav"

TARDIS:AddPart(PART)