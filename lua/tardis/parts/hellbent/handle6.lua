local PART={}
PART.ID = "hellbenthandle6"
PART.Name = "Hell Bent TARDIS Handle 6"
PART.Model = "models/doctorwho1200/hellbent/handle6.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hellbent/handle.wav"

TARDIS:AddPart(PART)