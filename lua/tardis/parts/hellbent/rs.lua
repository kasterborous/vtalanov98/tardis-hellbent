local PART={}
PART.ID = "hellbentrs"
PART.Name = "Hell Bent TARDIS Rotary Switch"
PART.Model = "models/doctorwho1200/hellbent/rotaryswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/rotaryswitch.wav"

TARDIS:AddPart(PART)