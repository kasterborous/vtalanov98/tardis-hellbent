local PART={}
PART.ID = "hellbentswitch10"
PART.Name = "Hell Bent TARDIS Switch 10"
PART.Model = "models/doctorwho1200/hellbent/switch10.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/hellbent/switch.wav"

TARDIS:AddPart(PART)