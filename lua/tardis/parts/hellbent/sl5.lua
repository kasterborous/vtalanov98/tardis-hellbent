local PART={}
PART.ID = "hellbentsl5"
PART.Name = "Hell Bent TARDIS Small Lever 5"
PART.Model = "models/doctorwho1200/hellbent/smalllever5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/smalllever.wav"

TARDIS:AddPart(PART)