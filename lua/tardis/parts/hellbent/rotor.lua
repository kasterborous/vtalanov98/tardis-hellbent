local PART={}
PART.ID = "hellbentrotor"
PART.Name = "Hell Bent TARDIS Time Rotor"
PART.Model = "models/doctorwho1200/hellbent/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

PART.Animate = true

PART.AnimateOptions = {
	Type = "travel",
	Speed = 0.12,
	PoseParameter = "rotor",
	NoPowerFreeze = false,
	ReturnAfterStop = true,
}

PART.ExtraAnimations = {
	spin = {
		Type = "idle",
		Speed = 0.04,
		PoseParameter = "rotorspinonly",
		NoPowerFreeze = true,
		ReturnAfterStop = false,
	}
}

TARDIS:AddPart(PART)