local PART={}
PART.ID = "hellbentintdoors"
PART.Name = "Hell Bent TARDIS Interior Doors"
PART.Model = "models/doctorwho1200/hellbent/intdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.5

PART.Sound = "doctorwho1200/hellbent/door.wav"
PART.SoundPos = Vector(20,128,45)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn(), true)
	end

	function PART:Toggle( bEnable, ply )
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
		self:SetOn(bEnable)
		self:SetCollide(not bEnable, true)
	end
end

TARDIS:AddPart(PART)