local PART={}
PART.ID = "hellbentrs8"
PART.Name = "Hell Bent TARDIS Rotary Switch 8"
PART.Model = "models/doctorwho1200/hellbent/rotaryswitch8.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/rotaryswitch.wav"

TARDIS:AddPart(PART)