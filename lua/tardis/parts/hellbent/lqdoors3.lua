local PART={}
PART.ID = "hellbentlqdoors3"
PART.Name = "Hell Bent TARDIS Living Quarters Doors 3"
PART.Model = "models/doctorwho1200/hellbent/lqdoors3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5

PART.SoundOff = "doctorwho1200/hellbent/lqdoorsclose.wav"
PART.SoundOn = "doctorwho1200/hellbent/lqdoorsopen.wav"
PART.SoundPos = Vector(-718.547, 287.084, 37.943)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end
end

TARDIS:AddPart(PART)