local PART={}
PART.ID = "hellbentswitch3"
PART.Name = "Hell Bent TARDIS Switch 3"
PART.Model = "models/doctorwho1200/hellbent/switch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/hellbent/switch.wav"

TARDIS:AddPart(PART)