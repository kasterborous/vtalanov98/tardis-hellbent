local PART={}
PART.ID = "hellbentsl11"
PART.Name = "Hell Bent TARDIS Small Lever 11"
PART.Model = "models/doctorwho1200/hellbent/smalllever11.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/smalllever.wav"

TARDIS:AddPart(PART)