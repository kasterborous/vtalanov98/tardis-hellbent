local PART={}
PART.ID = "hellbentsl"
PART.Name = "Hell Bent TARDIS Small Lever"
PART.Model = "models/doctorwho1200/hellbent/smalllever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/smalllever.wav"

TARDIS:AddPart(PART)