local PART={}
PART.ID = "hellbenthandle5"
PART.Name = "Hell Bent TARDIS Handle 5"
PART.Model = "models/doctorwho1200/hellbent/handle5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hellbent/handle.wav"

TARDIS:AddPart(PART)