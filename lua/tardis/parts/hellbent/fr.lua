local PART={}
PART.ID = "hellbentfr"
PART.Name = "Hell Bent TARDIS Fast Return Switch"
PART.Model = "models/doctorwho1200/hellbent/fastreturn.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctorwho1200/hellbent/button.wav"

TARDIS:AddPart(PART)