local PART={}
PART.ID = "hellbentrs4"
PART.Name = "Hell Bent TARDIS Rotary Switch 4"
PART.Model = "models/doctorwho1200/hellbent/rotaryswitch4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/rotaryswitch.wav"

TARDIS:AddPart(PART)