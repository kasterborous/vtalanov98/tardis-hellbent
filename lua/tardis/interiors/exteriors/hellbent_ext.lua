local E = {
	ID = "hellbent_tt_capsule",
	Base = "base",
	Name = "Hell Bent",
	Category = "Exteriors.Categories.TTCapsules",

	Model="models/doctorwho1200/hellbent/exterior.mdl",
	Mass=5000,
	Portal={
		pos=Vector(16.76,0,52.22),
		ang=Angle(0,0,0),
		width=30,
		height=88,
		thickness = 25,
		inverted = true,
	},
	Fallback={
		pos=Vector(44,0,7),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=false,
	},
	Sounds={
		Teleport={
			demat="doctorwho1200/hellbent/demat.wav",
			mat="doctorwho1200/hellbent/mat.wav"
		},
		Lock="doctorwho1200/hellbent/lock.wav",
		Door={
			enabled=true,
			open="doctorwho1200/hellbent/doorext_open.wav",
			close="doctorwho1200/hellbent/doorext_close.wav",
		},
		FlightLoop="doctorwho1200/hellbent/flight_loop.wav",
	},
	Parts={
		door={
			model="models/doctorwho1200/hellbent/doorsext.mdl",
			posoffset=Vector(-3,0,0),
			angoffset=Angle(0,0,0)
		},
	}
}

TARDIS:AddExterior(E)